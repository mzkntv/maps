import L from "leaflet";
import "leaflet/dist/leaflet.css";

// Instructions to other classes how be an argument for 'addMarker'
export interface Mappable {
  location: {
    lat: number;
    lng: number;
  };
  markerContent(): string;
  color: string;
}

export class CustomMap {
  private map: L.Map;

  constructor(mapDiv: string) {
    this.map = L.map(document.getElementById(mapDiv) as HTMLElement).setView(
      [0, 0],
      1
    );
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(this.map);
  }

  addMarker(mappable: Mappable): void {
    const marker = L.marker({
      lat: mappable.location.lat,
      lng: mappable.location.lng,
    }).addTo(this.map);

    marker.bindPopup(mappable.markerContent()).addTo(this.map);
  }
}
