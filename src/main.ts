import "./style.css";
import { CustomMap } from "./classes/CustomMap";
import { User } from "./classes/User";
import { Company } from "./classes/Company";

const app = document.querySelector<HTMLDivElement>("#app")!;

const user = new User();
const company = new Company();

const map = new CustomMap("map");
map.addMarker(user);
map.addMarker(company);

app.innerHTML = `
  <h1>Hello Vite!</h1>
  <a href="https://vitejs.dev/guide/features.html" target="_blank">Documentation</a>
`;
